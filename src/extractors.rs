use super::demo::*;
use failure::Error;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::io::{Read, Seek};

const RECORD_RE: &str = r"^//CTS RECORD SET ([\d:.]+) (\d+\.\d+) (\d+\.\d+) (\d+) (.*)";

pub trait Extractor<T: Read + Seek> {
    fn extract(&mut self) -> Result<Option<Box<dyn Extracted>>, Error>;
}

pub trait Extracted {
    fn start(&self) -> f32;
    fn stop(&self) -> f32;
    fn fmt_args(&self) -> HashMap<String, String>;
}

pub struct SimpleExtractor<'pr, T: Read + Seek> {
    pub reader: &'pr mut PacketReader<T>,
    pub start: f32,
    pub stop: f32,
}

pub struct SimpleExtracted {
    start: f32,
    stop: f32,
}

impl Extracted for SimpleExtracted {
    fn start(&self) -> f32 {
        self.start
    }

    fn stop(&self) -> f32 {
        self.stop
    }

    fn fmt_args(&self) -> HashMap<String, String> {
        let mut hm = HashMap::new();
        hm.insert(String::from("start"), self.start().to_string());
        hm.insert(String::from("stop"), self.stop().to_string());
        hm
    }
}

impl<'pr, T: Read + Seek> Extractor<T> for SimpleExtractor<'pr, T> {
    fn extract(&mut self) -> Result<Option<Box<dyn Extracted>>, Error> {
        Ok(Some(Box::new(SimpleExtracted {
            start: self.start,
            stop: self.stop,
        })))
    }
}

pub struct CTSRecordExtractor<'pr, T: Read + Seek> {
    pub reader: &'pr mut PacketReader<T>,
    pub start_margin: f32,
    pub stop_margin: f32,
    pub max_pos: u8,
}

#[derive(Debug)]
pub struct CTSRecord {
    human_readable_time: String,
    start: f32,
    stop: f32,
    dur: f32,
    pos: u8,
    crypto_idfp: String,
}

impl CTSRecord {
    pub fn try_from_str(s: &str, start_margin: f32, stop_margin: f32) -> Option<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(RECORD_RE).unwrap();
        }
        RE.captures(s).map(|caps| {
            let start: f32 = caps[2].parse().unwrap();
            let dur: f32 = caps[3].parse().unwrap();
            CTSRecord {
                human_readable_time: String::from(&caps[1]),
                start: start - start_margin,
                stop: start + dur + stop_margin,
                dur,
                pos: caps[4].parse().unwrap(),
                crypto_idfp: String::from(&caps[5]),
            }
        })
    }
}

impl Extracted for CTSRecord {
    fn start(&self) -> f32 {
        self.start
    }
    fn stop(&self) -> f32 {
        self.stop
    }

    fn fmt_args(&self) -> HashMap<String, String> {
        let mut hm = HashMap::new();
        hm.insert(String::from("start"), self.start().to_string());
        hm.insert(String::from("stop"), self.stop().to_string());
        hm.insert(String::from("pos"), self.pos.to_string());
        hm.insert(String::from("record"), self.human_readable_time.clone());
        hm.insert(String::from("record_f"), self.dur.to_string());
        hm.insert(String::from("crypto_idfp"), self.crypto_idfp.clone());
        hm
    }
}

impl<'pr, T: Read + Seek> Extractor<T> for CTSRecordExtractor<'pr, T> {
    fn extract(&mut self) -> Result<Option<Box<dyn Extracted>>, Error> {
        let mut rec: Option<CTSRecord> = None;
        self.reader.reset()?;
        self.reader.read_cdtrack()?;
        while let Some(i) = self.reader.next() {
            let packet = i?;
            if let DemoPacketContents::ServerPacket(SVC::Stufftext(s)) = packet.packet {
                if let Some(new_rec) =
                    CTSRecord::try_from_str(&s, self.start_margin, self.stop_margin)
                {
                    if new_rec.pos <= self.max_pos {
                        rec = match rec {
                            Some(old_rec) => {
                                if old_rec.dur < new_rec.dur {
                                    Some(old_rec)
                                } else {
                                    Some(new_rec)
                                }
                            }
                            _ => Some(new_rec),
                        };
                    }
                }
            }
        }
        if let Some(r) = rec {
            Ok(Some(Box::new(r)))
        } else {
            Ok(None)
        }
    }
}

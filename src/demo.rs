use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use failure::Error;
use std::io::{Cursor, ErrorKind, Read, Seek, SeekFrom, Write};
use std::str;

const DEMOMSG_CLIENT_TO_SERVER: usize = 0x8000_0000;
const SVC_DISCONNECT: u8 = 2;
const SVC_TIME: u8 = 7;
const SVC_STUFFTEXT: u8 = 9;
// const SVC_DOWNLOADDATA: u8 = 50;

#[derive(Debug)]
pub struct DemoPacket {
    pub angles: [f32; 3],
    pub packet: DemoPacketContents,
    // store the original data, cause I'm too lazy to write functions
    // to serialize Rust types...
    raw: Option<Vec<u8>>,
}

impl DemoPacket {
    pub fn write<T: Write>(&self, mut writer: T) -> Result<(), Error> {
        writer.write_i32::<LittleEndian>(self.raw.as_ref().unwrap().len() as i32)?;
        for i in &self.angles {
            writer.write_f32::<LittleEndian>(*i)?;
        }
        writer.write_all(self.raw.as_ref().unwrap())?;
        Ok(())
    }

    pub fn make_stufftext(stufftext: String) -> Self {
        let mut data = Vec::new();
        data.push(SVC_STUFFTEXT);
        data.extend_from_slice(stufftext.as_bytes());
        data.push(0);
        Self {
            angles: [0., 0., 0.],
            packet: DemoPacketContents::ServerPacket(SVC::Stufftext(stufftext)),
            raw: Some(data),
        }
    }

    pub fn make_slowmo(slowmo: u8) -> Self {
        let cmd = format!("\n//CUTMARK\nslowmo {}\n", slowmo);
        Self::make_stufftext(cmd)
    }

    pub fn make_videostart() -> Self {
        let cmd = String::from("\n//CUTMARK\nstartvideo\n");
        Self::make_stufftext(cmd)
    }

    pub fn make_videostop() -> Self {
        let cmd = String::from("\n//CUTMARK\nstopvideo\n");
        Self::make_stufftext(cmd)
    }

    pub fn make_disconnect() -> Self {
        let data = vec![SVC_DISCONNECT];
        Self {
            angles: [0., 0., 0.],
            packet: DemoPacketContents::ServerPacket(SVC::Disconnect),
            raw: Some(data),
        }
    }
}

#[derive(Debug)]
pub enum DemoPacketContents {
    ClientPacket,
    ServerPacket(SVC),
}

#[derive(Debug)]
pub enum SVC {
    Time(f32),
    Stufftext(String),
    Other(u8),
    Disconnect,
}

pub struct PacketReader<T: Read + Seek> {
    file: T,
}

fn process_string(input: &[u8]) -> Result<&str, Error> {
    let maybe_bytes = input.split(|x| *x == b'\0').nth(0);
    Ok(match maybe_bytes {
        Some(bytes) => str::from_utf8(bytes)?,
        None => "",
    })
}

impl<T: Read + Seek> PacketReader<T> {
    pub fn new(file: T) -> Self {
        Self { file }
    }

    pub fn reset(&mut self) -> Result<(), Error> {
        self.file.seek(SeekFrom::Start(0))?;
        Ok(())
    }

    pub fn read_cdtrack(&mut self) -> Result<Vec<u8>, Error> {
        let mut buf = [0; 1];
        let mut res = Vec::new();
        loop {
            self.file.read_exact(&mut buf)?;
            if buf[0] == b'\n' {
                return Ok(res);
            } else {
                res.push(buf[0])
            }
        }
    }

    pub fn read_packet(&mut self) -> Result<Option<DemoPacket>, Error> {
        let length = match self.file.read_i32::<LittleEndian>() {
            Ok(l) => l as usize,
            Err(e) => {
                if e.kind() == ErrorKind::UnexpectedEof {
                    return Ok(None);
                } else {
                    return Err(Error::from(e));
                }
            }
        };
        let mut angles: [f32; 3] = [0.; 3];
        for i in angles.iter_mut() {
            *i = self.file.read_f32::<LittleEndian>()?;
        }
        if length & DEMOMSG_CLIENT_TO_SERVER > 0 {
            let new_length = length & (!DEMOMSG_CLIENT_TO_SERVER);
            let mut buf: Vec<u8> = vec![0; new_length];
            self.file.read_exact(&mut buf)?;
            Ok(Some(DemoPacket {
                angles,
                packet: DemoPacketContents::ClientPacket,
                raw: Some(buf),
            }))
        } else {
            let mut buf: Vec<u8> = vec![0; length];
            self.file.read_exact(&mut buf)?;
            let type_ = buf[0];
            let packet = match type_ {
                SVC_TIME => {
                    let mut cursor = Cursor::new(&buf[1..]);
                    SVC::Time(cursor.read_f32::<LittleEndian>()?)
                }
                SVC_DISCONNECT => SVC::Disconnect,
                SVC_STUFFTEXT => {
                    let s = process_string(&buf[1..])?;
                    SVC::Stufftext(String::from(s))
                }
                // SVC_DOWNLOADDATA => SVC::DownloadData(),
                _ => SVC::Other(type_),
            };
            Ok(Some(DemoPacket {
                angles,
                packet: DemoPacketContents::ServerPacket(packet),
                raw: Some(buf),
            }))
        }
    }
}

impl<T: Read + Seek> Iterator for PacketReader<T> {
    type Item = Result<DemoPacket, Error>;
    fn next(&mut self) -> Option<Self::Item> {
        let packet = self.read_packet();
        match packet {
            Ok(Some(p)) => Some(Ok(p)),
            Ok(None) => None,
            Err(e) => Some(Err(e)),
        }
    }
}
